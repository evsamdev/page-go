/**
 * Created by Evgeny on 5/04/15.
 */

var markers = [];
var isAuthenticated = false;
var currentPageId;
var currentPageLink;
var privacy = 2;
var isLocalUser = false;

var messages = {
    "Welcome": "Welcome",
    "A brand new app.": "A brand new app.",
    "Error.Passport.Password.Invalid": "Password is invalid!",
    "Error.Passport.Password.Wrong": "Password is wrong!",
    "Error.Passport.Password.NotSet": "You haven't set a password yet!",
    "Error.Passport.Username.NotFound": "Username is wrong!",
    "Error.Passport.User.Exists": "This username is already taken.",
    "Error.Passport.Email.NotFound": "That email doesn't seem right",
    "Error.Passport.Email.Missing": "You need to supply an email-address",
    "Error.Passport.Email.Exists": "This email already exists.",
    "Error.Passport.Username.Missing": "You need to supply a username",
    "Error.Passport.Password.Missing": "You haven't set a password yet!",
    "Error.Passport.Generic": "Something went wrong!"
};


$(document).ready(function() {

    $("#publish_spinner").hide();

    var loginLink = $(".show_auth_form").attr("href");

    if(loginLink === "/logout"){
        isAuthenticated = true;
    }

    currentPageId = $("#page_id").val();
    currentPageLink = $("#page_link").val();
    if($("#local_user").val()==='true') {
        isLocalUser = true;
    }else if($("#local_user").val()==='toChangePassword'){
        isLocalUser = true;
        $("#update_spinner").hide();
        $("#update_alert").hide();
        var user_modal = UIkit.modal("#user_form");
        user_modal.show();
        console.log("Show User");
    }else{
        isLocalUser = false;
    }

    console.log(isLocalUser);

    if(currentPageId>0){

        var absLink = "https://evsam.net/"+currentPageLink;

        $("#active_link_tag").show();
        $(".active_link").text(absLink).attr("href",absLink).attr("target","_blank");
        $("#publish_button").text("Update");

    }else{
        $("#active_link_tag").hide();
    }


    if(!isAuthenticated) {

        if($("#welcome").length>0 && $.cookie('welcome')==undefined){

            var modal = UIkit.modal("#welcome");

            modal.show();
        }

    }

    $("#start_now").click(function (){


        modal.hide();

        console.log("Start Now");

        $.cookie('welcome', 'isShown', { expires: 365 });

        return false;

    });

    $(".show_auth_form").click(function (){

        if(isAuthenticated){
            return;
        }

        UIkit.offcanvas.hide();

        $("#login_spinner").hide();
        $("#login_alert").hide();

        var auth_modal = UIkit.modal("#auth_form");
        auth_modal.show();

        console.log("Show Auth");

        return false;

    });

    $(".username_button").click(function (){


        console.log("isAuthenticated: "+isAuthenticated);
        console.log("isLocalUser: "+isLocalUser);

        if(!isAuthenticated){
            return false;
        }

        if(!isLocalUser){
            return false;
        }

        UIkit.offcanvas.hide();

        $("#update_spinner").hide();
        $("#update_alert").hide();

        var user_modal = UIkit.modal("#user_form");
        user_modal.show();

        console.log("Show User");

        return false;

    });

    $("#login_button").click( function(){


        var username = $("#username_field").val();
        var password = $("#password_field").val();

        loginRequest(username,password);

        return false;

    });


    $("#register_button").click( function(){


        var username = $("#reg_username_field").val();
        var email = $("#reg_email_field").val();
        var password = $("#reg_password_field").val();
        var verify_password = $("#reg_verpassword_field").val();

        if(username.length<1){

            $("#login_alert").show().text("Please enter your username.");
            return;

        }


        var regexp = /^[a-zA-Z0-9-_]+$/;

        if(!username.match(regexp)){

            $("#login_alert").show().text("Use letters, numbers and dashes.");
            return;

        }

        if(username.length>25){

            $("#login_alert").show().text("Too long username (max 25).");
            return;

        }

        if(email.length<5){

            $("#login_alert").show().text("Please enter your email address.");
            return;
        }


        if(password.length<6){

            $("#login_alert").show().text("Too short password (min 6).");
            return;
        }

        if(password!=verify_password){

            $("#login_alert").show().text("Passwords don't match!");
            return;
        }

        registerRequest(username, email, password);

        return false;

    });

    $("#update_user_button").click( function(){


        var username = $("#update_username_field").val();
        var email = $("#update_email_field").val();
        var password = $("#update_password_field").val();
        var verify_password = $("#update_verpassword_field").val();

        console.log("Change Password");

        if(password.length<6){

            $("#update_alert").show().text("Too short password. (min 6)");
            return;
        }


        if(password && password!=verify_password){

            $("#update_alert").show().text("Passwords don't match!");
            return;
        }


        updateUserRequest(username, email, password);

        return false;

    });

    $("#reset_button").click( function(){

        var email = $("#reset_email_field").val();

        if(email.length==0){

            $("#login_alert").show().text("Please enter your email address.");
            return;
        }


        resetRequest(email);

        return false;

    });


    $("#open_register_form").click( function(){

        $("#login_alert").hide();
        $("#register_form").show();
        $("#login_form").hide();

        return false;

    });

    $("#open_login_form").click( function(){

        $("#login_alert").hide();
        $("#register_form").hide();
        $("#login_form").show();

        return false;

    });

    $("#forgot_password_button").click( function(){

        $("#login_alert").hide();
        $("#register_form").hide();
        $("#login_form").hide();
        $("#reset_form").show();

        return false;

    });

    $("#open_login_form_1").click( function(){

        $("#login_alert").hide();
        $("#register_form").hide();
        $("#reset_form").hide();
        $("#login_form").show();

        return false;

    });

    $("#publish_button").click( function(){

        var html = $('.uk-htmleditor-preview').children().eq(0).html();
        var currentCode = UIkit.htmleditor.currentCode;

        var title = $('#page_title').val();
        console.log(html);

        if(title==""){


            UIkit.Utils.scrollToElement(UIkit.$("body"),{
                duration: 1000,
                transition: 'easeOutExpo',
                offset: 0,
                complete: function(){$("#page_title").focus();}
            });

            UIkit.notify("<div class='uk-text-center'>Please enter Title</div>");



            return false;
        }

        if(!currentCode){

            UIkit.notify("<div class='uk-text-center'>Your page is blank</div>");

            return false;
        }

        var link = $('#page_link').val();
        var key = $('#page_key').val();

        sendPublishRequest(html,title,currentCode,link, key);
        return false;

    });

    $(".privacy_selector").click(function(event){

        var elId = event.target.id;

        var newPrivacy = elId.substr(8,1);
        console.log("privacy: "+newPrivacy);
        if(newPrivacy=="3" && !isAuthenticated){
            return false;
        }else{

            privacy = newPrivacy;
        }

        $("#privacy_value").text($(event.target).text());

        //return false;
    });



});

function deletePage(deletePadeTagId){

    if(!confirm("Are you sure you want to delete this Page?")){
        return false;
    }

    var link = deletePadeTagId.substr(14,12);

    console.log("delete link: " + link);

    $("#"+deletePadeTagId).hide();
    $("#delete_spinner_"+link).show();

    var data = {link:link};

    $.ajax({
            type: "POST",
            url: "/delete",
            data: data
        }
    ).done(function (result) {

            console.log("delete page result: " + JSON.stringify(result));

            if(result.status===0){

                $("#page_"+link).hide();

            }else{

                console.log("page error: " + JSON.stringify(result));

            }

        }).fail(function(jqXHR,textStatus){

            console.log("Request status: " +jqXHR.status);

            $("#delete_spinner_"+link).hide();
            $("#"+deletePadeTagId).show();
            alert("Request failed: " + textStatus);

        });


    return false;
}

function sendPublishRequest(body,title, code, link, key){

    var data = {
        'body':body,
        'title': title,
        'code':code,
        'privacy':privacy,
        'link':link,
        'key':key
    };

    console.log("currentPageId:");
    console.log(currentPageId);

    if(currentPageId>0){

        data.pageId = currentPageId;
    }

    console.log("data: " +JSON.stringify(data));

    $("#publish_spinner").show();
    $("#publish_button").hide();
    $.ajax({
            type: "POST",
            url: "/page",
            data: data
        }
    ).done(function (result) {

            $("#publish_spinner").hide();
            $("#publish_button").show();
            console.log("page result: " + JSON.stringify(result));

            if(result.status===0){

                currentPageId = result.pageId;
                currentPageLink = result.pageLink;

                var absLink = "https://evsam.net/"+currentPageLink;

                $("#active_link_tag").show();
                $(".active_link").text(absLink).attr("href",absLink).attr("target","_blank");
                $("#publish_button").text("Update");

            }else{

                console.log("page error: " + JSON.stringify(result));

            }


        }).fail(function(jqXHR,textStatus){

            console.log("Request status: " +jqXHR.status);

            $("#publish_spinner").hide();
            $("#publish_button").show();
            alert("Request failed: " + textStatus);

        });


}

function loginRequest(username, password){

    $("#login_spinner").show();
    $("#login_alert").hide();

    var data = {
        'identifier':username,
        'password':password
        };

    console.log("data: " +JSON.stringify(data));

    $.ajax({
            type: "POST",
            url: "/auth/local",
            data:data
        }
    ).done(function (result) {

            console.log("login: " + JSON.stringify(result));

            //var result = JSON.parse(data);

            $("#login_spinner").hide();

            if(result.status===0){

                location.reload();

            }else{

                //alert("Error: " + result.error);
                if(messages.hasOwnProperty(result.error)) {
                    $("#login_alert").show().text(messages[result.error]);
                }else{
                    $("#login_alert").show().text("Authentication error!");
                }
            }



        }).fail(function(jqXHR,textStatus){

            console.log("Request status: " +jqXHR.status);

            alert("Request failed: " + textStatus);
            $("#login_spinner").hide();

        });


}



function resetRequest(email){

    $("#login_spinner").show();
    $("#login_alert").hide();

    var data = {
        'email':email
    };

    console.log("data: " +JSON.stringify(data));

    $.ajax({
            type: "POST",
            url: "/auth/resetpassword",
            data:data
        }
    ).done(function (result) {

            console.log("login: " + JSON.stringify(result));

            //var result = JSON.parse(data);

            $("#login_spinner").hide();

            if(result.status===0){

                var auth_modal = UIkit.modal("#auth_form");
                auth_modal.hide();

                UIkit.notify("<div class='uk-text-center'>"+result.message+"</div>");

                $("#update_password_field").val("");
                $("#update_verpassword_field").val("");

            }else{

                //alert("Error: " + result.error);

                $("#login_alert").show().text(result.error);

            }



        }).fail(function(jqXHR,textStatus){

            console.log("Request status: " +jqXHR.status);

            alert("Request failed: " + textStatus);
            $("#login_spinner").hide();

        });


}


function registerRequest(username, email, password){

    $("#login_spinner").show();
    $("#login_alert").hide();

    var data = {
        'username':username,
        'email':email,
        'password':password
    };

    console.log("data: " +JSON.stringify(data));

    $.ajax({
            type: "POST",
            url: "/auth/local/register",
            data:data
        }
    ).done(function (result) {

            console.log("login: " + JSON.stringify(result));

            //var result = JSON.parse(data);

            $("#login_spinner").hide();

            if(result.status===0){

                location.reload()

            }else{

                //alert("Error: " + result.error);
                if(messages.hasOwnProperty(result.error)) {
                    $("#login_alert").show().text(messages[result.error]);
                }else{
                    $("#login_alert").show().text("Register error!");
                }
            }



        }).fail(function(jqXHR,textStatus){

            console.log("Request status: " +jqXHR.status);

            alert("Request failed: " + textStatus);
            $("#login_spinner").hide();

        });


}

function updateUserRequest(username, email, password){

    $("#update_spinner").show();
    $("#update_alert").hide();

    var data = {
        'username':username,
        'email':email,
        'password':password
    };

    console.log("data: " +JSON.stringify(data));

    $.ajax({
            type: "POST",
            url: "/auth/updateuser",
            data:data
        }
    ).done(function (result) {

            console.log("login: " + JSON.stringify(result));

            $("#update_spinner").hide();

            if(result.status===0){

                var user_modal = UIkit.modal("#user_form");
                user_modal.hide();

                $("#update_password_field").val("");
                $("#update_verpassword_field").val("");

            }else{

                //alert("Error: " + result.error);
                if(messages.hasOwnProperty(result.error)) {
                    $("#update_alert").show().text(messages[result.error]);
                }else{
                    $("#update_alert").show().text("Update error!");
                }
            }



        }).fail(function(jqXHR,textStatus){

            console.log("Request status: " +jqXHR.status);

            alert("Request failed: " + textStatus);
            $("#update_spinner").hide();

        });


}


function activateUploader(){


    var markerManager = UIkit.htmleditor.markerManager;
    var markers = markerManager.markers;

    console.log("--Markers--");

    for (var key in markers) {
        if (markers.hasOwnProperty(key)) {
            console.log(key+" "+ markers[key]);
        }
    }

     $(".upload-drop").each( function (i, obj){
            var uploadPrefix = "image_upload";
            var id = uploadPrefix + '_' + parseInt(i+1);

            $(this).attr("id",id);

            console.log("$uploader: "+ i +" -- "+ obj);
            var progressbar = $(obj).next('.progressbar');
            console.log("progressbar:");
            console.log(progressbar);
            var bar = progressbar.find('.uk-progress-bar'),
                settings = {

                    action: '/images/upload', // upload url
                    allow: '*.(jpg|gif|png)', // allow only images

                    param: ["avatar"],

                    loadstart: function () {
                        bar.css("width", "0%").text("0%");
                        progressbar.removeClass("uk-hidden");
                        //console.log("loadstart");
                    },

                    progress: function (percent) {
                        percent = Math.ceil(percent);
                        bar.css("width", percent + "%").text(percent + "%");
                        //console.log("percent: "+ percent);

                    },

                    allcomplete: function (response, xhr, files) {

                        bar.css("width", "100%").text("100%");


                        setTimeout(function () {
                            progressbar.addClass("uk-hidden");
                        }, 250);

                        var result = JSON.parse(response);
                        console.log("uploaded files: " + JSON.stringify(result));

                        markerManager.handleImgUpload(id,result.file);

                    }


                };
            var uploadSelect =  $(obj).find(".upload-select");

            var select = UIkit.uploadSelect(uploadSelect, settings),
                drop = UIkit.uploadDrop($(obj), settings);


    });


};
