var User = {

  attributes: {
      username  : { type: 'alphanumericdashed', unique: true, size: 100 },
      email     : { type: 'email',  unique: true, size: 100 },
      reset_token  : { type: 'string', unique: true, size: 100 },
      passports : { collection: 'Passport', via: 'user' },
      pages : { collection: 'Page', via: 'user' }
  }

};



module.exports = User;
