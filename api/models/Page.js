/**
* Page.js
*
* @description :: Main entity that contains user created content page.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

var Page = {

    schema: true,

  attributes: {

      user: { model: 'User', required: true },
      link : { type: 'string' },
      body: { type: 'string' },
      code: { type: 'string' },
      title : { type: 'string' },
      privacy : { type: 'integer' },
      expiration : { type: 'integer' },
      status :  { type: 'integer' }

  }
};

module.exports = Page;
