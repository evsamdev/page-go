/**
 * MainController
 *
 * @description :: Server-side logic for managing mains
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var bcrypt = require('bcryptjs');
var salt = "$2a$10$m.44p4/tphv6QP3MDyKYjO";

module.exports = {

    PAGE_LIMIT: 20,
    privacyValues : {

        "2":"Public By Link ",
        "3":"Private "

    },
    privacy:function (req, res) {

        res.view('privacy');
    },
    /**
     * CommentController.index()
     */
    index: function (req, res) {

        var statusCode = 200;

        var result = {
            status: statusCode
        };

        result.isLocalUser = false;

        var isAuthenticated = req.session.authenticated;

        var self = this;

        if (isAuthenticated) {

            //console.log("User:");

            //console.log(req.user);
            //console.log(req.session);

            result.username = req.user.username;
            result.email = req.user.email;
            result.loginLink = "/logout";
            result.loginTitle = "Logout";

            Passport.findOne({
                 user     : req.user.id
            }, function (err, passport) {
                if (err) {
                    console.log("ERROR! Passport findOne error:");
                    console.log(err);
                    res.status(404);
                    return res.view('404', {error: err});
                }

                if (passport) {


                    //console.log("passport.protocol: "+passport.protocol);

                    if (passport.protocol=="local") {

                        result.isLocalUser = true;
                        //console.log(result.isLocalUser);
                    }

                    result.isAuthenticated = isAuthenticated;

                    result.title = "";
                    result.code = "";
                    result.pageId = 0;
                    result.pageLink= generateLink();
                    result.privacyValue = self.privacyValues["2"];


                    var keySource = "3547623562987876"+result.pageLink;

                    var hash = bcrypt.hashSync(keySource, salt);

                    result.pageKey = hash;

                    // Set status code and view locals
                    res.status(result.status);
                    for (var key in result) {
                        res.locals[key] = result[key];
                    }
                    // And render view
                    return res.view('homepage');

                }else{

                    console.log("ERROR! Passport not found error:");
                    res.status(404);
                    return res.view('404', {error: "No Passport"});


                }

            });



        }else{

            res.redirect("/");

        }


    },
    resetpassword: function (req, res){


        var statusCode = 200;

        var result = {
            status: statusCode
        };

        result.isLocalUser = true;

        var self = this;

        var email = req.param("email");
        var resetToken = req.param("reset_token");

        if (!resetToken || resetToken===null) {

            console.log("WARNING! No reset token");
            console.log("email: "+email);

            res.status(404);
            return res.view('404', {error: "Your Reset Link is invalid"});
        }


        User.findOne({
            email  : email,
            reset_token : resetToken
        }, function (err, user) {
            if (err) {

                console.log("ERROR! Reset token. User.findOne error:");
                console.log(err);

                res.status(404);
                return res.view('404', {error: err});
            }

            if (user) {

                req.login(user, function (err) {

                    if (err) {

                        console.log("ERROR! Reset token. req.login() error:");
                        console.log(err);
                        res.status(404);
                        return res.view('404', {error: "Your Reset Link is invalid"});
                    }

                    // Mark the session as authenticated to work with default Sails sessionAuth.js policy
                    req.session.authenticated = true;


                    //console.log("User:");

                    //console.log(req.user);
                    //console.log(req.session);

                    result.isLocalUser = "toChangePassword";

                    result.isAuthenticated = true;

                    result.username = user.username;
                    result.email = user.email;
                    result.loginLink = "/logout";
                    result.loginTitle = "Logout";

                    result.title = "";
                    result.code = "";
                    result.pageId = 0;
                    result.pageLink= generateLink();
                    result.privacyValue = self.privacyValues["2"];

                    var keySource = "3547623562987876"+result.pageLink;

                    var hash = bcrypt.hashSync(keySource, salt);

                    result.pageKey = hash;

                    // Set status code and view locals
                    res.status(result.status);
                    for (var key in result) {
                        res.locals[key] = result[key];
                    }
                    // And render view
                    return res.view('homepage');



                });

            }else{

                console.log("WARNING! Your Reset Link is invalid");
                console.log("email: "+email);
                console.log("reset_token: "+resetToken);
                res.status(404);
                return res.view('404', {error: "Your Reset Link is invalid"});

            }
        });



    },
    page: function(req, res){

        var link = req.param("link");
        var incomingPageKey = req.param("key");

        if(!link || !incomingPageKey){

            console.log("WARNING! Page. No Link. No Key");
            console.log("link: "+link);
            console.log("incomingPageKey: "+incomingPageKey);

            res.status(403);
            return res.json({error: 'No Required Parameters'});
        }

        var keySource = "3547623562987876"+link;

        var hash = bcrypt.hashSync(keySource, salt);

        if(hash!==incomingPageKey){

            console.log("WARNING! Page. Invalid Key");
            console.log("link: "+link);
            console.log("incomingPageKey: "+incomingPageKey);
            console.log("hash: "+hash);

            res.status(403);
            return res.json({error: 'Not Valid Request'});

        }

        var isAuthenticated = req.session.authenticated;
        if(!isAuthenticated){

            res.status(403);
            var result = {
                status: -1
            };
            return res.json(result);

        }

        var query={};
        if(req.user) {
            query.user = req.user.id;
        }else{
            res.status(403);
            var result = {
                status: -1
            };
            return res.json(result);
        }

        query.title = req.param("title");
        query.status = 1;

        var body = req.param("body");

        var SCRIPT_REGEX = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi;
        while (SCRIPT_REGEX.test(body)) {
            body = body.replace(SCRIPT_REGEX, "");
            console.log("Script injection detected!");
        }

        query.body = body;

        query.code = req.param("code");
        query.privacy = parseInt(req.param("privacy"));
        query.expiration = 0;

        var pageId = req.param("pageId");
        if(pageId){

            var findQuery = {id : pageId, link: link};

            //console.log(query);

            Page.update(findQuery, query ,function (err, pages) {

                //console.log(pages[0]);

                if (err || !pages || pages.length==0) {

                    console.log("ERROR! Page.update. Page not found");
                    console.log("link: "+link);
                    console.log("pageId: "+pageId);
                    console.log(err);

                    var result = {

                        status: -1,
                        error: err

                    };
                    res.status(404);
                    return res.json(result);

                }
                var page = pages[0];
                var result = {

                    status: 0,
                    pageId: page.id,
                    pageLink: page.link

                };
                return res.json(result);


            });


        }else {

            query.link = link;

            Page.findOne({
                link : link
            }, function (err, page) {
                if (err) {

                    console.log("ERROR! Page.findOne error");
                    console.log("link: "+link);
                    console.log("pageId: "+pageId);
                    console.log(err);

                    var result = {

                        status: -1,
                        error: err

                    };
                    res.status(404);
                    return res.json(result);
                }

                if(page){

                    //console.log(query);

                    console.log("WARNING! Page found with zero PageID");
                    console.log("link: "+link);
                    console.log("pageId: "+pageId);
                    console.log(page);

                    page.body = query.body;
                    page.code =query.code;
                    page.privacy =query.privacy;
                    page.title = query.title;
                    page.status = 1;
                    page.user = query.user;

                    page.save(function(err){

                        console.log("ERROR! Page found with zero PageID. Page.save error");
                        console.log(page);
                        console.log(err);
                        if(err){
                            var result = {

                                status: -1,
                                error: err

                            };
                            res.status(404);
                            return res.json(result);
                        }

                        var result = {

                            status: 0,
                            pageId: page.id,
                            pageLink: page.link

                        };
                        return res.json(result);

                    });

                }else {


                    Page.create(query, function (err, page) {

                        if (err) {

                            console.log("ERROR! Page.create error");
                            console.log(query);
                            console.log(err);

                            var result = {

                                status: -1,
                                error: err

                            };
                            res.status(404);
                            return res.json(result);

                        }

                        var result = {

                            status: 0,
                            pageId: page.id,
                            pageLink: page.link

                        };
                        return res.json(result);


                    });

                }


            });



        }

    },

    getpage: function(req, res){

        var viewFilePath = "page";
        var statusCode = 200;
        var link = req.param("link");
        var result = {
            status: statusCode
        };

        //console.log("Link:"+link);

        if(link.length!==12 || link.indexOf(".") > -1){

            res.status(404);
            return res.view('404', {error: "Sorry, page doesn't exist!"});

        }
        //console.log("Passed Link:"+link);

        Page.findOne({
            link : link


        }, function (err, page) {
            if (err) {

                console.log("ERROR! Getpage. Page.findOne error");
                console.log("link: "+link);
                console.log(err);
                return res.json(err);
            }

            if(!page){
                res.status(404);
                return res.view('404', {error: "Sorry, page doesn't exist!"});
            }

            if(page.privacy==3){


                if(req.session.authenticated){

                     if(req.user.id !== page.user){
                         res.status(403);
                         return res.view('403', {error: 'Please use your login!'});
                     }

                }else {

                    res.status(403);
                    return res.view('403', {error: 'Please use your login!'});
                }

            }

            //console.log(page);

            result.title = page.title;
            result.body = page.body;
            result.fullLink = "https://evsam.net/"+page.link;
            result.privacy = page.privacy;

            res.status(result.status);
            for (var key in result) {
                res.locals[key] = result[key];
            }
            // And render view
            res.render(viewFilePath, result, function(err) {
                // If the view doesn't exist, or an error occured, send json
                if (err) {

                    console.log("ERROR! Getpage. res.render error");
                    console.log("link: "+link);
                    console.log(result);
                    console.log(err);
                    return res.json(err);
                }

                // Otherwise, serve the `views/mySpecialView.*` page
                res.render(viewFilePath);
            });


        });

    },
    deletepage: function(req, res){

        var isAuthenticated = req.session.authenticated;
        if(!isAuthenticated){

            res.status(403);
            var result = {
                status: -1
            };
            return res.json(result);

        }

        var link = req.param("link");

        //console.log("Link:"+link);
        //console.log("req.user.id:"+req.user.id);

        var findQuery = {
            link : link,
            user: req.user.id
        };

        var query = {status:2};

        //console.log(query);

        Page.update(findQuery, query ,function (err, pages) {


            if (err) {

                console.log("ERROR! deletepage. Page.update error");
                console.log("link: "+link);
                console.log(query);
                console.log(err);

                return res.json(err);
            }

            //console.log(pages);

            if(pages && pages.length>0){

                var result = {
                    status: 0
                };
                return res.json(result);

            }

            console.log("WARNING! deletepage. Page not found");
            console.log("link: "+link);
            console.log(query);
            console.log(err);

            res.status(404);
            var result = {
                status: -1
            };
            return res.json(result);

        });

    },

    editpage: function(req, res){

        var link = req.param("link");
        var statusCode = 200;
        var result = {
            status: statusCode
        };
        result.isLocalUser = false;
        //console.log("Link:"+link);

        var isAuthenticated = req.session.authenticated;

        var self = this;

        if (isAuthenticated) {

            //console.log("req.user.id:" + req.user.id);
            //console.log(req.user);

            result.username = req.user.username;
            result.email = req.user.email;
            result.loginLink = "/logout";
            result.loginTitle = "Logout";
            result.isAuthenticated = isAuthenticated;

            Passport.findOne({
                    user     : req.user.id
                }, function (err, passport) {
                if (err) {

                    console.log("ERROR! Editpage Passport findOne error:");
                    console.log(err);

                    res.status(404);
                    return res.view('404', {error: err});
                }

                if (passport) {

                    //console.log("passport.protocol: "+passport.protocol);

                    if (passport.protocol=="local") {

                        result.isLocalUser = true;
                        //console.log(result.isLocalUser);
                    }


                    Page.findOne({
                        link : link,
                        user: req.user.id
                    }, function (err, page) {
                        if (err) {

                            console.log("ERROR! Editpage. Page.findOne error:");
                            console.log(err);
                            return res.json(err);
                        }

                        if(!page){

                            console.log("WARNING! Editpage. Page.findOne page not found:");
                            console.log("link: " +link);
                            console.log("req.user.id: " +req.user.id);

                            res.status(404);
                            return res.view('404', {error: "Sorry, page doesn't exist!"});
                        }

                        // Set status code and view locals
                        res.status(result.status);

                        result.title = page.title;
                        result.code = page.code;
                        result.pageId = page.id;
                        result.pageLink = page.link;
                        var privacy = page.privacy+"";
                        result.privacyValue = self.privacyValues[privacy];

                        var keySource = "3547623562987876"+result.pageLink;

                        var hash = bcrypt.hashSync(keySource, salt);

                        result.pageKey = hash;

                        for (var key in result) {
                            res.locals[key] = result[key];
                        }
                        //console.log(page);
                        // And render view
                        return res.view("homepage");


                    });



                } else {

                    console.log("ERROR! Editpage Passport not found");
                    console.log("req.user.id:"+req.user.id);

                    res.status(404);
                    return res.view('404', {error: "No Passport"});


                }


            });



        }else{


            res.status(403);
            return res.view('403', {error: 'Please use your login!'});

        }

    },
    mainpage: function(req, res) {

        var isAuthenticated = req.session.authenticated;

        if (isAuthenticated) {
            var username = req.user.username;
            res.redirect("/public/"+username)
        }else{

            var statusCode = 200;

            var result = {
                status: statusCode
            };

            result.isLocalUser = false;
            result.isAuthenticated = false;
            result.pageList = [];
            result.prevP = 0;
            result.nextP = 0;
            result.username = "Guest";
            result.loginLink = "/";
            result.loginTitle = "Login";
            result.email = "";

            res.status(result.status);
            for (var key in result) {
                res.locals[key] = result[key];
            }

            res.view('pagelist');
        }

    },

    publiclist: function (req, res) {

        var statusCode = 200;

        var result = {
            status: statusCode
        };

        result.isLocalUser = false;

        var isAuthenticated = req.session.authenticated;

        var self = this;

        if (isAuthenticated) {

            //console.log(req.user);

            result.username = req.user.username;
            result.email = req.user.email;
            result.loginLink = "/logout";
            result.loginTitle = "Logout";

            Passport.findOne({
                user     : req.user.id
            }, function (err, passport) {
                if (err) {

                    console.log("ERROR! publiclist Passport findOne error:");
                    console.log(err);

                    res.status(404);
                    return res.view('404', {error: err});
                }

                if (passport) {

                    //console.log("passport.protocol: "+passport.protocol);

                    if (passport.protocol == "local") {

                        result.isLocalUser = true;
                        //console.log(result.isLocalUser);

                    }

                    result.isAuthenticated = isAuthenticated;

                    var author = req.param("author");
                    var p = req.param("p");
                    if(!p){
                        p=1;
                    }
                    p = parseInt(p);

                    var query;

                    result.isMyPages = false;

                    query = {where : {status: 1, privacy:1 }, sort:"updatedAt DESC"};


                    if(author){

                        if(author!==req.user.username){

                            console.log("ERROR! Other author:");
                            console.log("author:"+author);
                            console.log("req.user.username:"+req.user.username);
                            res.status(404);
                            return res.view('404', {error: "Sorry, page doesn't exist!"});
                        }
                        //console.log("Author: "+author);

                        User.findOne({username:author}).exec(function(err, authorUser) {

                            if (err) {

                                console.log("ERROR! publiclist User findOne error:");
                                console.log(err);

                                res.status(404);
                                return res.view('404', {error: err});
                            }

                            //console.log(authorUser);

                            if(!authorUser){

                                res.status(404);
                                return res.view('404', {error: "Sorry, page doesn't exist!"});

                            }

                            query = {where : { status: 1 , user: authorUser.id }, sort:"updatedAt DESC"};

                            result.isMyPages = true;


                            Page.find(query).populate('user').paginate({page: p, limit: self.PAGE_LIMIT}).exec(function(err, pageList) {

                                if (err) {

                                    console.log("ERROR! publiclist Page find error:");
                                    console.log(err);

                                    res.status(404);
                                    return res.view('404', {error: err});
                                }

                                //console.log(pageList);

                                result.pageList = pageList;
                                if(pageList.length<self.PAGE_LIMIT) {
                                    result.prevP = 0;
                                }else{
                                    result.prevP = p + 1;
                                }
                                result.nextP = p-1;

                                // Set status code and view locals
                                res.status(result.status);
                                for (var key in result) {
                                    res.locals[key] = result[key];
                                }
                                // And render view
                                return res.view('pagelist');

                            });



                        });



                    }else {

                        console.log("ERROR! No author:");
                        console.log("req.user.id:"+req.user.id);
                        res.status(404);
                        return res.view('404', {error: "No author"});

                    }


                }else{

                    console.log("ERROR! Editpage Passport not found");
                    console.log("req.user.id:"+req.user.id);

                    res.status(404);
                    return res.view('404', {error: "No Passport"});s

                }
            });


        }else {

            res.redirect("/")

        }

    }

	
};

function generateLink() {

    //console.log(Date.now() + Math.floor(Math.random() * 10000000000000));
    var sum = Date.now() + Math.floor(Math.random() * 10000000000000);
    var str = sum.toString();
    var buf = new Buffer(str).toString('base64');
    buf = buf.substr(5, 12).toLowerCase();
    //console.log(buf);

    return buf;

}

//Ghost pass  DerhyipPe567