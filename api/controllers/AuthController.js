/**
 * Authentication Controller
 *
 * This is merely meant as an example of how your Authentication controller
 * should look. It currently includes the minimum amount of functionality for
 * the basics of Passport.js to work.
 */

var nodemailer = require('nodemailer');

var AuthController = {
  /**
   * Render the login page
   *
   * The login form itself is just a simple HTML form:
   *
      <form role="form" action="/auth/local" method="post">
        <input type="text" name="identifier" placeholder="Username or Email">
        <input type="password" name="password" placeholder="Password">
        <button type="submit">Sign in</button>
      </form>
   *
   * You could optionally add CSRF-protection as outlined in the documentation:
   * http://sailsjs.org/#!documentation/config.csrf
   *
   * A simple example of automatically listing all available providers in a
   * Handlebars template would look like this:
   *
      {{#each providers}}
        <a href="/auth/{{slug}}" role="button">{{name}}</a>
      {{/each}}
   *
   * @param {Object} req
   * @param {Object} res
   */
  login: function (req, res) {
    var strategies = sails.config.passport
      , providers  = {};

    // Get a list of available providers for use in your templates.
    Object.keys(strategies).forEach(function (key) {
      if (key === 'local') {
        return;
      }

      providers[key] = {
        name: strategies[key].name
      , slug: key
      };
    });

    // Render the `auth/login.ext` view
    res.view({
      providers : providers
    , errors    : req.flash('error')
    });
  },

  /**
   * Log out a user and return them to the homepage
   *
   * Passport exposes a logout() function on req (also aliased as logOut()) that
   * can be called from any route handler which needs to terminate a login
   * session. Invoking logout() will remove the req.user property and clear the
   * login session (if any).
   *
   * For more information on logging out users in Passport.js, check out:
   * http://passportjs.org/guide/logout/
   *
   * @param {Object} req
   * @param {Object} res
   */
  logout: function (req, res) {
    req.logout();
    
    // mark the user as logged out for auth purposes
    req.session.authenticated = false;


    res.redirect('/');
  },

  /**
   * Render the registration page
   *
   * Just like the login form, the registration form is just simple HTML:
   *
      <form role="form" action="/auth/local/register" method="post">
        <input type="text" name="username" placeholder="Username">
        <input type="text" name="email" placeholder="Email">
        <input type="password" name="password" placeholder="Password">
        <button type="submit">Sign up</button>
      </form>
   *
   * @param {Object} req
   * @param {Object} res
   */
  register: function (req, res) {
    res.view({
      errors: req.flash('error')
    });
  },

  /**
   * Create a third-party authentication endpoint
   *
   * @param {Object} req
   * @param {Object} res
   */
  provider: function (req, res) {
    passport.endpoint(req, res);
  },

  /**
   * Create a authentication callback endpoint
   *
   * This endpoint handles everything related to creating and verifying Pass-
   * ports and users, both locally and from third-aprty providers.
   *
   * Passport exposes a login() function on req (also aliased as logIn()) that
   * can be used to establish a login session. When the login operation
   * completes, user will be assigned to req.user.
   *
   * For more information on logging in users in Passport.js, check out:
   * http://passportjs.org/guide/login/
   *
   * @param {Object} req
   * @param {Object} res
   */
  callback: function (req, res) {

      var provider = req.param("provider");
      //console.log("provider: "+provider);

      function tryAgain (err) {

          // Only certain error messages are returned via req.flash('error', someError)
          // because we shouldn't expose internal authorization errors to the user.
          // We do return a generic error and the original request body.
          var flashError = req.flash('error')[0];

          var error;
          if (err && !flashError ) {
              error = err;
          } else if (flashError) {
              error = flashError;
          }


          var denied = req.param('denied');
          //console.log("denied: "+denied);
          if(denied){

              return res.redirect("/");
          }

          //console.log("Error:");
          //console.log(error);
         // If an error was thrown, redirect the user to the
         // login, register or disconnect action initiator view.
         // These views should take care of rendering the error messages.
          var callbackError = req.param('error_code');
          //console.log("callbackError: "+callbackError);
          if(callbackError){

              return res.redirect("/");
          }


          if(provider=='facebook' || provider=='twitter'){

              console.log("ERROR! Auth callback Error:");
              console.log(error);

              return res.redirect("/");
          }


        var result = {

            status: -1,
            error: error

        };
        res.json(result);


    }

    passport.callback(req, res, function (err, user, challenges, statuses) {
      if (err || !user) {
        return tryAgain(err);//tryAgain(challenges);
      }

      req.login(user, function (err) {
        if (err) {
          return tryAgain(err);
        }
        
        // Mark the session as authenticated to work with default Sails sessionAuth.js policy
        req.session.authenticated = true;

          var provider = req.param("provider");
          //console.log("provider: "+provider);
          if(provider=="twitter"){

              return res.redirect("/");
          }

          if(provider=="facebook"){

              return res.redirect("/");
          }
        // Upon successful login, send the user to the homepage were req.user
        // will be available.
          var result = {

              status: 0,
              username: req.user.username,
              email: req.user.email

          };
        res.json(result);
      });
    });
  },

  /**
   * Disconnect a passport from a user
   *
   * @param {Object} req
   * @param {Object} res
   */
  disconnect: function (req, res) {
    passport.disconnect(req, res);
  },

    updateuser: function (req, res) {

        var password = req.param("password");

        Passport.update({
            protocol: 'local'
            ,user: req.user.id
        }, {password: password}, function (err, passports) {

            if (err) {

                console.log("ERROR! Auth updateuser Error:");
                console.log(error);
                res.status(404);
                return res.json(err);
            }

            req.user.reset_token = null;

            req.user.save(function(err){

                console.log("ERROR! Auth. updateuser. user.save Error:");
                console.log(err);

            });

            var result = {
                status: 0
            };
            return res.json(result);

        });

    },

    resetpassword: function (req, res){

        var email = req.param("email");

        User.findOne({
            email : email
        }, function (err, user) {
            if (err) {
                return res.json(err);
            }

            if (!user) {

                return res.json({status:-1, error: "Sorry, user with this email doesn't exist!"});
            }


            Passport.findOne({
                user     : user.id
            }, function (err, passport) {
                if (err) {

                    console.log("ERROR! Auth. resetpassword. Passport.findOne Error:");
                    console.log(err);

                    return res.json({status:-1, error: "Sorry, user with this email doesn't exist!"});
                }

                if (passport) {

                    if (passport.protocol!=="local") {

                        return res.json({status:-1, error: "Sorry, user with this email doesn't exist!"});

                    }


                    var smtpTransport = nodemailer.createTransport({
                        service: 'SendGrid',
                        auth: {
                            user: 'samsonoveu',
                            pass: 'Solaris5'
                        }
                    });

                    var bcrypt = require('bcryptjs');
                    var salt = bcrypt.genSaltSync(10);

                    var keySource = Date.now() + Math.floor(Math.random() * 10000000000000) + "";

                    var hash = bcrypt.hashSync(keySource, salt);
                    var buf = new Buffer(hash).toString('base64');
                    buf = buf.substr(0, 64);

                    user.reset_token = buf;
                    user.save(function (err) {

                        console.log("ERROR! Auth. resetpassword. user.save Error:");
                        console.log(err);
                    });

                    var link = "https://evsam.net/reset_password?email=" + user.email + "&reset_token=" + buf;

                    var mailOptions = {
                        to: user.email,
                        from: 'Page Go ✔ <admin@pagego.net>',
                        subject: 'Change Password',
                        text: 'Hello,\n\n' + 'This is a Reset Password link for your Page Go account ' + user.username + '.\n' + link

                    };
                    smtpTransport.sendMail(mailOptions, function (err) {


                        if (err) {

                            console.log("ERROR! Auth. resetpassword. sendMail Error:");
                            console.log(err);
                            return res.json({status: -1, error: "Something went wrong, please try again."});
                        }

                        return res.json({
                            status: 0,
                            message: "The activation link has been sent to your email address!"
                        });


                    });

                }else{

                    console.log("ERROR! Auth. resetpassword. Passport not found Error:");
                    console.log(err);
                    return res.json({status:-1, error: "Sorry, user with this email doesn't exist!"});

                }
            });

        });

    }

};

module.exports = AuthController;
