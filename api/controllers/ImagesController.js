/**
 * ImagesController
 *
 * @description :: Server-side logic for managing images
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */


module.exports = {



    index: function (req,res){
            return res.json({'status':'Not allowed'});
        /*
        res.writeHead(200, {'content-type': 'text/html'});
        res.end(
            '<form action="http://localhost:1337/images/upload" enctype="multipart/form-data" method="post">'+
            '<input type="text" name="title"><br>'+
            '<input type="file" name="avatar" multiple="multiple"><br>'+
            '<input type="submit" value="Upload">'+
            '</form>'
        )
        */
    },

    /**
     * `ImagesController.upload()`
     */

    upload: function (req, res) {

        //console.log(req);

        if(req.method === 'GET')
            return res.json({'status':'GET not allowed'});
        //	Call to /upload via GET is error

        var uploadFile = req.file('avatar');

        var path = require('path').resolve(sails.config.appPath, 'img');

        uploadFile.upload({
            dirname: path
        },function (err, uploadedFiles) {
            if (err) return res.negotiate(err);

            if( uploadedFiles.length>0) {
                var file = uploadedFiles[0];
                var fd = file.fd;
                var fileName = fd.split("/").pop();
                fileName = "http://"+sails.config.host+"/img/"+fileName;
                return res.json({status: 0, file: fileName});
            }else{
                return res.json({status: -1});
            }
        });


    }

};

