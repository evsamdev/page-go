/**
 * grunt/pipeline.js
 *
 * The order in which your css, javascript, and template files should be
 * compiled and linked from your views and static HTML files.
 *
 * (Note that you can take advantage of Grunt-style wildcard/glob/splat expressions
 * for matching multiple files.)
 */



// CSS files to inject in order
//
// (if you're using LESS with the built-in default config, you'll want
//  to change `assets/styles/importer.less` instead.)
var cssFilesToInject = [
  //'styles/**/*.css'
    "/styles/uikit/uikit.almost-flat.css",
    "/styles/uikit/components/form-file.almost-flat.css",
    "/styles/uikit/components/htmleditor.almost-flat.css",
    "/styles/uikit/components/notify.almost-flat.css",
    "/styles/uikit/components/placeholder.almost-flat.css",
    "/styles/uikit/components/progress.almost-flat.css",
    "/styles/uikit/components/upload.almost-flat.css",
    "/js/frameworks/vendor/codemirror/codemirror.css",
    "/js/frameworks/vendor/codemirror/show-hint.css",
    "/js/frameworks/vendor/highlight/highlight.css"

];


// Client-side javascript files to inject in order
// (uses Grunt-style wildcard/glob/splat expressions)
var jsFilesToInject = [
  
  // Load sails.io before everything else
  //'js/dependencies/sails.io.js',

  // Dependencies like jQuery, or Angular are brought in here
  //'js/dependencies/**/*.js',

  // All of the rest of your client-side js files
  // will be injected here in no particular order.
  //'js/**/*.js'
    "/js/frameworks/vendor/jquery.js",
    "/js/frameworks/vendor/jquery.cookie.js",
    "/js/frameworks/uikit/js/uikit.js",
    "/js/frameworks/vendor/highlight/highlight.js",
    "/js/frameworks/vendor/codemirror/codemirror.js",
    "/js/frameworks/vendor/marked.js",
    "/js/frameworks/uikit/js/core/alert.js",
    "/js/frameworks/uikit/js/core/button.js",
    "/js/frameworks/uikit/js/core/core.js",
    "/js/frameworks/uikit/js/core/cover.js",
    "/js/frameworks/uikit/js/core/dropdown.js",
    "/js/frameworks/uikit/js/core/grid.js",
    "/js/frameworks/uikit/js/core/modal.js",
    "/js/frameworks/uikit/js/core/nav.js",
    "/js/frameworks/uikit/js/core/offcanvas.js",
    "/js/frameworks/uikit/js/core/scrollspy.js",
    "/js/frameworks/uikit/js/core/smooth-scroll.js",
    "/js/frameworks/uikit/js/core/switcher.js",
    "/js/frameworks/uikit/js/core/tab.js",
    "/js/frameworks/uikit/js/core/toggle.js",
    "/js/frameworks/uikit/js/core/touch.js",
    "/js/frameworks/uikit/js/core/utility.js",
    "/js/frameworks/uikit/js/components/htmleditor.js",
    "/js/frameworks/uikit/js/components/upload.js",
    "/js/frameworks/uikit/js/components/notify.js"

];


// Client-side HTML templates are injected using the sources below
// The ordering of these templates shouldn't matter.
// (uses Grunt-style wildcard/glob/splat expressions)
//
// By default, Sails uses JST templates and precompiles them into
// functions for you.  If you want to use jade, handlebars, dust, etc.,
// with the linker, no problem-- you'll just want to make sure the precompiled
// templates get spit out to the same file.  Be sure and check out `tasks/README.md`
// for information on customizing and installing new tasks.
var templateFilesToInject = [
  //'templates/**/*.html'
];




// Prefix relative paths to source files so they point to the proper locations
// (i.e. where the other Grunt tasks spit them out, or in some cases, where
// they reside in the first place)

module.exports.cssFilesToInject = cssFilesToInject.map(function(path) {
  return '.tmp/public/' + path;
});
module.exports.jsFilesToInject = jsFilesToInject.map(function(path) {
  return '.tmp/public/' + path;
});
module.exports.templateFilesToInject = templateFilesToInject.map(function(path) {
  return 'assets/' + path;
});
